import {createLogger} from "redux-logger";
import {applyMiddleware, createStore} from "redux";
import rootReducer from '../reducers'
import thunkMiddleware from "redux-thunk";

export function configureStore(initialSate) {
const logger = createLogger();
    return createStore(
        rootReducer,
        initialSate,
        applyMiddleware(thunkMiddleware, logger));
}