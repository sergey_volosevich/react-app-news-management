import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';

import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

const languages = ['en', 'ru'];

const options =
    {
        order: ['querystring', 'cookie', 'localStorage', 'navigator', 'htmlTag', 'path', 'subdomain'],

        lookupQuerystring: 'lng',
        lookupCookie: 'i18next',
        lookupLocalStorage: 'i18nextLng',
        lookupFromPathIndex: 0,
        lookupFromSubdomainIndex: 0,

        caches: ['localStorage', 'cookie'],
        excludeCacheFor: ['cimode'],

        cookieMinutes: 10,
        cookieDomain: 'http://localhost:3000/',

        htmlTag: document.documentElement,

        checkWhitelist: true
    };

i18n
    .use(Backend)

    .use(LanguageDetector)

    .use(initReactI18next)

    .init({
        fallbackLng: 'en',
        debug: true,
        whitelist: languages,
        detection: options,
        interpolation: {
            escapeValue: false,
        }
    });

export default i18n;