import * as React from "react";
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux";
import * as authActions from "../actions/AuthenticationActions";
import {connect} from "react-redux";
import {getI18n, withTranslation} from 'react-i18next';


class Header extends React.Component {

    handleClick(lang) {
        getI18n().changeLanguage(lang);
    }

    render() {
        const user = this.props.user.user;
        const t = this.props.t;
        if (user){
            console.log(typeof this.props.user.user.username);
        }
        return (
            <div className="header">
                <div className="row">
                    <div className="column side">
                        <p className="app-name">{t('Header.siteName')}</p>
                    </div>
                    <div className="column middle">
                        <ul className="lang">
                            <li><a href="#lng=en" onClick={() => this.handleClick('en')}>EN</a></li>
                            <li><a href="#lng=ru" onClick={() => this.handleClick('ru')}>RU</a></li>
                            {user ? (<li>{t('Header.welcomeMessage', {user: this.props.user.user.username})}
                            </li>) : null}
                        </ul>

                    </div>
                    <div className="column side">
                        <ul className="register">
                            {user ? <li><Link to='/logout' onClick={
                                    () => {
                                        this.props.authActions.logOut()
                                    }
                                }>{t('Header.logout')}</Link></li> :
                                <React.Fragment>
                                    <li><Link to='/login'>{t('Header.loginLink')}</Link></li>
                                    <li><Link to='/registration'>{t('Header.registrationLink')}</Link></li>
                                </React.Fragment>
                            }
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    }
}

const HeaderComponent = withTranslation()(Header);

export default connect(mapStateToProps, mapDispatchToProps)(HeaderComponent)