import React from "react";
import {bindActionCreators} from "redux";
import * as newsActions from "../actions/NewsActions";
import {connect} from "react-redux";
import NewsComponentCard from "../components/news/NewsComponentCard";

class NewsCard extends React.Component {
    render() {
        const newsActions = this.props.newsActions;
        const news = this.props.news;
        const id = Number(this.props.match.params.id);
        const user = this.props.user;
        return (
            <NewsComponentCard id={id} user={user} content={news} newsActions={newsActions}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        news: state.news,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(NewsCard)