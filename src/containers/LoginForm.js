import React from "react";
import {bindActionCreators} from "redux";
import * as authActions from "../actions/AuthenticationActions";
import {connect} from "react-redux";
import LoginFormComponent from "../components/login-logout/LoginFormComponent";

class LoginForm extends React.Component {

    render() {
        const authActions = this.props.authActions;
        return (
            <LoginFormComponent location={this.props.location} authActions={authActions}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)