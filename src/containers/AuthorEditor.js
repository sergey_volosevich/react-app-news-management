import React from "react";
import {bindActionCreators} from "redux";
import * as authorActions from "../actions/AuthorActions";
import {connect} from "react-redux";
import AuthorRedactorComponent from "../components/author/AuthorRedactorComponent";

class AuthorEditor extends React.Component {
    render() {
        const authorActions = this.props.authorActions;
        const authors = this.props.authors;
        return (
            <AuthorRedactorComponent authors={authors} authorActions={authorActions}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        authors: state.authors
    }
}

function mapDispatchToProps(dispatch) {
    return {
        authorActions: bindActionCreators(authorActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorEditor)