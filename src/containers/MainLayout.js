import * as React from "react";
import {Route, Switch} from "react-router";
import DashBoardComponent from "../components/dashboard/DashBoardComponent";
import NewsEditor from "./NewsEditor";
import LogoutComponent from "../components/login-logout/LogoutComponent";
import {PrivateRoute} from "../router/PrivateRoute";
import LoginForm from "./LoginForm";
import AuthorEditor from "./AuthorEditor";
import TagEditor from "./TagEditor";
import PageNotFoundComponent from "../components/PageNotFoundComponent";
import IndexPage from "./IndexPage";
import NewsCard from "./NewsCard";

export default class MainLayout extends React.Component {
    render() {
        return (
            <div className="row content">
                <div className="column side">
                    {localStorage.getItem("userInfo") ? <DashBoardComponent/> : null}
                </div>
                <div className="column middle">
                    <Switch>
                        <Route exact path="/" component={IndexPage}/>
                        <Route exact path="/logout" component={LogoutComponent}/>
                        <PrivateRoute exact path='/addAuthor' component={AuthorEditor}/>
                        <PrivateRoute exact path='/addNews' component={NewsEditor}/>
                        <PrivateRoute exact path='/editNews/:id' component={NewsEditor}/>
                        <PrivateRoute exact path='/addTag' component={TagEditor}/>
                        <Route path='/news/:id' render={(props) => <NewsCard {...props}/>}/>
                        <Route exact path="/login" component={LoginForm}/>
                        <Route path='*' component={PageNotFoundComponent}/>
                    </Switch>
                </div>
                <div className="column side">
                </div>
            </div>
        )
    }
}