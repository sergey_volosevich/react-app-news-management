import React from "react";
import {bindActionCreators} from "redux";
import * as tagActions from "../actions/TagActions";
import {connect} from "react-redux";
import TagRedactorComponent from "../components/tag/TagRedactorComponent";

class TagEditor extends React.Component {
    render() {
        const tags = this.props.tags;
        const tagActions = this.props.tagActions;
        return (
            <TagRedactorComponent tags={tags} tagActions={tagActions}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        tags: state.tags,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        tagActions: bindActionCreators(tagActions, dispatch),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(TagEditor)