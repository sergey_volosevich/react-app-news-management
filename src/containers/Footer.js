import * as React from "react";

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">Copyright EPAM 2020, All Right Reserved</div>
        )
    }
}

export default Footer