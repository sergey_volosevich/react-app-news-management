import React from "react";
import {bindActionCreators} from "redux";
import * as tagActions from "../actions/TagActions";
import * as authorActions from "../actions/AuthorActions";
import * as newsActions from "../actions/NewsActions"
import {connect} from "react-redux";
import NewsFormComponent from "../components/news/NewsFormComponent";
import NewsRefactorComponent from "../components/news/NewsRefactorComponent";

class NewsEditor extends React.Component {

    componentDidMount() {
        this.props.tagActions.getTags();
        this.props.authorActions.getAuthors();
        if (this.props.match.params.id) {
            this.props.newsActions.getNewsById(this.props.match.params.id);
        }
    }

    render() {
        const tagActions = this.props.tagActions;
        const authorActions = this.props.authorActions;
        const newsActions = this.props.newsActions;
        const tags = this.props.tags;
        const authors = this.props.authors;
        const news = this.props.news;
        const id = Number(this.props.match.params.id);
        if (id && news.content.length!==0) {
            const foundNews = news.content.find(item => item.id === id);
            return(
                <NewsRefactorComponent
                    news={foundNews}
                    newsActions={newsActions}
                    tagActions={tagActions}
                    authorActions={authorActions}
                    tags={tags}
                    authors={authors}
                    />
            )
        }
        return (
            <NewsFormComponent
                tags={tags}
                authors={authors}
                authorActions={authorActions}
                tagActions={tagActions}
                saveNews={newsActions.saveNews}
            />
        )
    }
}

function mapStateToProps(state) {
    return {
        news: state.news,
        tags: state.tags,
        authors: state.authors
    }
}

function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
        tagActions: bindActionCreators(tagActions, dispatch),
        authorActions: bindActionCreators(authorActions, dispatch)
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(NewsEditor)