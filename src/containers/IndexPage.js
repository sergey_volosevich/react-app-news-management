import React from "react";
import {bindActionCreators} from "redux";
import * as newsActions from "../actions/NewsActions";
import * as tagActions from "../actions/TagActions";
import * as authorActions from "../actions/AuthorActions";
import {connect} from "react-redux";
import SearchComponent from "../components/search/SearchComponent";
import NewsComponentLoader from "../components/news/NewsComponentLoader";
import queryString from 'query-string';

class IndexPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            limit: this.getLimit(),
            currentPage: this.getCurrentPage()
        };
        this.handleClickLimit = this.handleClickLimit.bind(this);
        this.handleClickLink = this.handleClickLink.bind(this);
    }

    getLimit() {
        let limit = localStorage.getItem('limit');
        if (limit) {
            return Number(limit);
        }
        localStorage.setItem('limit', '5');
        return 5;
    }

    getCurrentPage() {
        let search = this.props.location.search;
        if (search) {
            let parsedQuery = queryString.parse(search);
            console.log(parsedQuery);
            let split = parsedQuery.pag.split(';');
            console.log(split);
            const regexp = /page:\d/;
            let page = split.find((item => regexp.test(item)));
            if (page) {
                return Number(page.split(':')[1]);
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }

    handleClickLimit(newLimit) {
        localStorage.setItem('limit', newLimit);
        this.setState({
            limit: newLimit,
            currentPage: 1
        });
    }

    handleClickLink(newCurrentPage) {
        this.setState({currentPage: newCurrentPage});
    }

    render() {
        const newsActions = this.props.newsActions;
        const tagActions = this.props.tagActions;
        const authorActions = this.props.authorActions;
        const {limit, currentPage} = this.state;
        const news = this.props.news;
        const user = this.props.user;
        const tags = this.props.tags;
        const pagination = this.props.news.pagination;
        const authors = this.props.authors;
        const history = this.props.history;
        let parse = queryString.parse(this.props.location.search);
        return (
            <div>
                <SearchComponent tags={tags}
                                 authors={authors}
                                 tagActions={tagActions}
                                 limit={limit}
                                 history={history}
                                 setCurrentPage={this.handleClickLink}
                                 currentPage={currentPage}
                                 authorActions={authorActions}
                                 newsActions={newsActions}/>
                <NewsComponentLoader search={parse}
                                     currentPage={currentPage}
                                     limit={limit}
                                     handleClickLink={this.handleClickLink}
                                     handleClickLimit={this.handleClickLimit}
                                     pagination={pagination}
                                     user={user}
                                     content={news}
                                     newsActions={newsActions}/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        news: state.news,
        tags: state.tags,
        authors: state.authors
    }
}

function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
        tagActions: bindActionCreators(tagActions, dispatch),
        authorActions: bindActionCreators(authorActions, dispatch)
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(IndexPage)