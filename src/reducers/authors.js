import {
    DELETE_AUTHOR_BY_ID_ERROR,
    DELETE_AUTHOR_BY_ID_SUCCESS,
    GET_AUTHORS_ERROR,
    GET_AUTHORS_REQUEST,
    GET_AUTHORS_SUCCESS,
    POST_AUTHOR_ERROR,
    POST_AUTHOR_SUCCESS,
    PUT_AUTHOR_ERROR,
    PUT_AUTHOR_SUCCESS
} from "../actions/constants/Authors";

const initialState = {
    fetching: false,
    content: [],
    message: null
};

export function authors(state = initialState, action) {
    function updateAuthor() {
        const {message, result} = action.payload;
        const index = state.content.findIndex((item => item.id === result.id));
        state.content.splice(index, 1, result);
        return {...state, content: state.content, message: message};
    }

    function deleteAuthor() {
        const {id, message} = action.payload;
        const index = state.content.findIndex(item => item.id === id);
        state.content.splice(index, 1);
        return {...state, message: message, content: state.content};
    }

    switch (action.type) {
        case GET_AUTHORS_REQUEST:
            return {...state, fetching: true};
        case GET_AUTHORS_SUCCESS:
            return {...state, fetching: false, content: action.payload};
        case PUT_AUTHOR_ERROR:
        case POST_AUTHOR_ERROR:
        case GET_AUTHORS_ERROR:
        case DELETE_AUTHOR_BY_ID_ERROR:
            return {...state, fetching: false, message: action.payload};
        case DELETE_AUTHOR_BY_ID_SUCCESS:
            return deleteAuthor();
        case PUT_AUTHOR_SUCCESS:
            return updateAuthor();
        case POST_AUTHOR_SUCCESS:
            const {message, result} = action.payload;
            state.content.unshift(result);
            return {...state, message: message, content: state.content};
        default:
            return state;
    }
}