import {
    DELETE_TAG_BY_ID_ERROR,
    DELETE_TAG_BY_ID_SUCCESS,
    GET_ALL_TAGS_ERROR,
    GET_ALL_TAGS_REQUEST,
    GET_ALL_TAGS_SUCCESS,
    POST_TAG_ERROR,
    POST_TAG_SUCCESS,
    PUT_TAG_ERROR,
    PUT_TAG_SUCCESS
} from "../actions/constants/Tags";

const initialState = {
    fetching: false,
    content: [],
    message: null
};

export function tags(state = initialState, action) {

    function updateTag() {
        const {message, result} = action.payload;
        const index = state.content.findIndex((item => item.id === result.id));
        state.content.splice(index, 1, result);
        return {...state, content: state.content, message: message};
    }

    function deleteTag() {
        const {id, message} = action.payload;
        const index = state.content.findIndex(item => item.id === id);
        state.content.splice(index, 1);
        return {...state, message: message, content: state.content};
    }

    switch (action.type) {
        case GET_ALL_TAGS_REQUEST:
            return {...state, fetching: true};
        case GET_ALL_TAGS_SUCCESS:
            return {...state, fetching: false, content: action.payload};
        case PUT_TAG_ERROR:
        case POST_TAG_ERROR:
        case GET_ALL_TAGS_ERROR:
        case DELETE_TAG_BY_ID_ERROR:
            return {...state, fetching: false, message: action.payload};
        case DELETE_TAG_BY_ID_SUCCESS:
            return deleteTag();
        case PUT_TAG_SUCCESS:
            return updateTag();
        case POST_TAG_SUCCESS:
            const {message, result} = action.payload;
            state.content.unshift(result);
            return {...state, message: message, content: state.content};
        default:
            return state;
    }
}
