import {LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, LOGOUT_USER} from "../actions/constants/Authentication";

const initialState = {
    user: JSON.parse(localStorage.getItem('userInfo'))
};

export function user(state = initialState, action) {
    switch (action.type) {
        case LOGIN_USER_REQUEST:
            return state;
        case LOGIN_USER_SUCCESS:
            return {...state, user: action.payload};
        case LOGOUT_USER:
            return {...state, user: action.payload};
        default:
            return state;
    }
}