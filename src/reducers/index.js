import {combineReducers} from "redux";
import {news} from "./news";
import {tags} from "./tags";
import {authors} from "./authors";
import {user} from "./user";

export default combineReducers(
    {
        news,
        tags,
        authors,
        user
    }
)