import {
    DELETE_NEWS_BY_ID_ERROR,
    DELETE_NEWS_BY_ID_REQUEST,
    DELETE_NEWS_BY_ID_SUCCESS,
    GET_FOUND_NEWS_ERROR,
    GET_FOUND_NEWS_REQUEST,
    GET_FOUND_NEWS_SUCCESS,
    GET_NEWS_BY_ID_ERROR,
    GET_NEWS_BY_ID_REQUEST,
    GET_NEWS_BY_ID_SUCCESS,
    GET_NEWS_ERROR,
    GET_NEWS_REQUEST,
    GET_NEWS_RESPONSE_HEADERS,
    GET_NEWS_SUCCESS,
    POST_NEWS_ERROR,
    POST_NEWS_SUCCESS,
    PUT_NEWS_ERROR
} from "../actions/constants/News";
import {PUT_AUTHOR_SUCCESS} from "../actions/constants/Authors";

const initialState = {
    fetching: false,
    content: [],
    message: null,
    pagination: {
        totalCount: 0
    }
};

export function news(state = initialState, action) {

    function deleteNews() {
        const {id, message} = action.payload;
        const index = state.content.findIndex(item => item.id === id);
        state.content.splice(index, 1);
        return {...state, message: message, content: state.content};
    }

    function updateNews() {
        const {message, result} = action.payload;
        const index = state.content.findIndex((item => item.id === result.id));
        state.content.splice(index, 1, result);
        return {...state, content: state.content, message: message};
    }

    switch (action.type) {
        case GET_NEWS_BY_ID_REQUEST:
            return {...state, fetching: true};
        case GET_NEWS_BY_ID_SUCCESS:
            const news = action.payload;
            if (state.content.length !== 0) {
                const index = state.content.findIndex((item => item.id === news.id));
                state.content.splice(index, 1, news);
                return {...state, content: state.content, fetching: false};
            } else {
                state.content.unshift(news);
                return {...state, content: state.content, fetching: false};
            }

        case PUT_AUTHOR_SUCCESS:
            return updateNews();
        case GET_FOUND_NEWS_REQUEST:
        case GET_NEWS_REQUEST:
            return {...state, fetching: true};
        case GET_NEWS_RESPONSE_HEADERS:
            return {
                ...state, fetching: false, pagination: {
                    totalCount: Number(action.payload)
                }
            };
        case DELETE_NEWS_BY_ID_REQUEST:
            return state;
        case GET_FOUND_NEWS_SUCCESS:
        case GET_NEWS_SUCCESS:
            return {...state, fetching: false, content: action.payload};
        case POST_NEWS_ERROR:
        case PUT_NEWS_ERROR:
        case GET_NEWS_BY_ID_ERROR:
        case GET_NEWS_ERROR:
        case DELETE_NEWS_BY_ID_ERROR:
        case GET_FOUND_NEWS_ERROR:
            return {...state, fetching: false, message: action.payload};
        case DELETE_NEWS_BY_ID_SUCCESS:
            return deleteNews();
        case POST_NEWS_SUCCESS:
            const {message, result} = action.payload;
            state.content.unshift(result);
            return {...state, message: message, content: state.content};
        default:
            return state;
    }
}