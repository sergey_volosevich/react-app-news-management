import {
    DELETE_TAG_BY_ID_ERROR,
    DELETE_TAG_BY_ID_REQUEST,
    DELETE_TAG_BY_ID_SUCCESS,
    GET_ALL_TAGS_ERROR,
    GET_ALL_TAGS_REQUEST,
    GET_ALL_TAGS_SUCCESS,
    POST_TAG_ERROR,
    POST_TAG_REQUEST,
    POST_TAG_SUCCESS,
    PUT_TAG_ERROR,
    PUT_TAG_REQUEST,
    PUT_TAG_SUCCESS
} from "./constants/Tags";

const API_URL = 'http://localhost:9080/news';

export function getTags() {
    return (dispatch) => {
        dispatch({
            type: GET_ALL_TAGS_REQUEST,
        });

        fetch(`${API_URL}/tags`)
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    console.error(res.json());
                    throw new Error(`Error to load tags from server. Response status code ${res.status}`);
                }
            })
            .then(result =>
                dispatch({
                    type: GET_ALL_TAGS_SUCCESS,
                    payload: result
                }))
            .catch(error => {
                console.error("Error to load tags from server.", error);
                dispatch({
                    type: GET_ALL_TAGS_ERROR,
                    payload: "Oops! Something go wrong. Can not load tags."
                })
            })
    }
}

export function deleteTag(id) {
    return (dispatch) => {
        dispatch({
            type: DELETE_TAG_BY_ID_REQUEST,
        });

        fetch(`${API_URL}/tags/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then(res => {
                if (res.status === 204) {
                    dispatch({
                        type: DELETE_TAG_BY_ID_SUCCESS,
                        payload: {
                            message: "tag successful deleted",
                            id: id
                        }
                    });
                    console.log(`tag with id  = ${id} successful deleted`);
                } else {
                    console.error(res.json());
                    throw new Error(`Error to delete tag. Response status code ${res.status}`)
                }
            })
            .catch(error => {
                console.error("Error to delete tag.", error);
                dispatch({
                    type: DELETE_TAG_BY_ID_ERROR,
                    payload: "Oops! Something go wrong. Can not delete tag."
                })
            })
    }
}

export function updateTag(tag) {
    return (dispatch) => {
        dispatch({
            type: PUT_TAG_REQUEST
        });
        fetch(`${API_URL}/tags/${tag.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(tag)
        })
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    console.error(res.json());
                    throw new Error(`Error to update tag with id = ${tag.id}. Response status code ${res.status}`)
                }
            })
            .then(result => {
                dispatch({
                    type: PUT_TAG_SUCCESS,
                    payload: {
                        result: result,
                        message: "tag successful updated"
                    }
                });
                console.log(`tag with id  = ${tag.id} successful updated`);
            })
            .catch(error => {
                console.error("Error to update tag.", error);
                dispatch({
                    type: PUT_TAG_ERROR,
                    payload: "Oops! Something go wrong. Can not update tag."
                })
            })
    }
}

export function saveTag(tag) {
    return (dispatch) => {
        dispatch({
            type: POST_TAG_REQUEST
        });

        fetch(`${API_URL}/tags`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(tag)
        })
            .then(res => {
                if (res.status === 201) {
                    return res.json();
                } else {
                    console.error(res.json());
                    throw new Error(`Error to save tag. Response status code ${res.status}`)
                }
            })
            .then(result => {
                dispatch({
                    type: POST_TAG_SUCCESS,
                    payload: {
                        result: result,
                        message: "tag successful saved"
                    }
                });
                console.log(`tag with name = ${tag.name} successful saved`);
            })
            .catch(error => {
                console.error("Error to save tag.", error);
                dispatch({
                    type: POST_TAG_ERROR,
                    payload: "Oops! Something go wrong. Can not save tag."
                })
            })
    }
}

