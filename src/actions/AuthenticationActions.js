import {LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, LOGOUT_USER} from "./constants/Authentication";

export function login(user) {
    return (dispatch) => {
        dispatch({
            type: LOGIN_USER_REQUEST
        });

        const userCredentials = {
            user: user,
            username: "Sergei"
        };

        const serialObj = JSON.stringify(userCredentials);
        localStorage.setItem('userInfo', serialObj);

        dispatch({
            type: LOGIN_USER_SUCCESS,
            payload: userCredentials
        });
    }
}

export function logOut() {
    return (dispatch) => {
        localStorage.removeItem("userInfo");
        dispatch({
            type: LOGOUT_USER,
            payload: null
        })
    }
}