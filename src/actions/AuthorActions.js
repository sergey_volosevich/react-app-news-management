import {
    DELETE_AUTHOR_BY_ID_ERROR,
    DELETE_AUTHOR_BY_ID_REQUEST,
    DELETE_AUTHOR_BY_ID_SUCCESS,
    GET_AUTHORS_ERROR,
    GET_AUTHORS_REQUEST,
    GET_AUTHORS_SUCCESS,
    POST_AUTHOR_ERROR,
    POST_AUTHOR_REQUEST,
    POST_AUTHOR_SUCCESS,
    PUT_AUTHOR_ERROR,
    PUT_AUTHOR_REQUEST,
    PUT_AUTHOR_SUCCESS
} from "./constants/Authors";

const API_URL = 'http://localhost:9080/news';


export function getAuthors() {
    return (dispatch) => {
        dispatch({
            type: GET_AUTHORS_REQUEST,
        });

        fetch(`${API_URL}/authors`)
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    console.error(res.json());
                    throw new Error(`Error to load authors from server. Response status code ${res.status}`)
                }
            })
            .then(result =>
                dispatch({
                    type: GET_AUTHORS_SUCCESS,
                    payload: result
                }))
            .catch(error => {
                console.error("Error to load authors from server.", error);
                dispatch({
                    type: GET_AUTHORS_ERROR,
                    payload: "Oops! Something go wrong. Can not load authors."
                })
            })
    }
}

export function deleteAuthor(id) {
    return (dispatch) => {
        dispatch({
            type: DELETE_AUTHOR_BY_ID_REQUEST,
        });

        fetch(`${API_URL}/authors/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then(res => {
                if (res.status === 204) {
                    dispatch({
                        type: DELETE_AUTHOR_BY_ID_SUCCESS,
                        payload: {
                            message: "author successful deleted",
                            id: id
                        }
                    });
                    console.log(`author with id = ${id} successful deleted`);
                } else {
                    console.error(res.json());
                    throw new Error(`Error to delete author. Response status code ${res.status}`)
                }
            })
            .catch(error => {
                console.error("Error to delete author.", error);
                dispatch({
                    type: DELETE_AUTHOR_BY_ID_ERROR,
                    payload: "Oops! Something go wrong. Can not delete author."
                })
            })
    }
}

export function updateAuthor(author) {
    return (dispatch) => {
        dispatch({
            type: PUT_AUTHOR_REQUEST
        });
        fetch(`${API_URL}/authors/${author.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(author)
        })
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    console.error(res.json());
                    throw new Error(`Error to update author with id = ${author.id}. Response status code ${res.status}`)
                }
            })
            .then(result => {
                dispatch({
                    type: PUT_AUTHOR_SUCCESS,
                    payload: {
                        result: result,
                        message: "author successful updated"
                    }
                });
                console.log(`author with id = ${author.id} successful updated`);
            })
            .catch(error => {
                console.error("Error to update author.", error);
                dispatch({
                    type: PUT_AUTHOR_ERROR,
                    payload: "Oops! Something go wrong. Can not update author."
                })
            })
    }
}

export function saveAuthor(author) {
    return (dispatch) => {
        dispatch({
            type: POST_AUTHOR_REQUEST
        });

        fetch(`${API_URL}/authors`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(author)
        })
            .then(res => {
                if (res.status === 201) {
                    return res.json();
                } else {
                    console.error(res.json());
                    throw new Error(`Error to save author. Response status code ${res.status}`);
                }
            })
            .then(result => {
                dispatch({
                    type: POST_AUTHOR_SUCCESS,
                    payload: {
                        result: result,
                        message: "author successful saved"
                    }
                });
                console.log(`author with name = ${author.name} successful saved`);
            })
            .catch(error => {
                console.error("Error to save author.", error);
                dispatch({
                    type: POST_AUTHOR_ERROR,
                    payload: "Oops! Something go wrong. Can not save author."
                })
            })
    }
}