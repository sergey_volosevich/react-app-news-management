import {
    DELETE_NEWS_BY_ID_ERROR,
    DELETE_NEWS_BY_ID_REQUEST,
    DELETE_NEWS_BY_ID_SUCCESS, GET_FOUND_NEWS_ERROR, GET_FOUND_NEWS_REQUEST, GET_FOUND_NEWS_SUCCESS,
    GET_NEWS_BY_ID_ERROR,
    GET_NEWS_BY_ID_REQUEST,
    GET_NEWS_BY_ID_SUCCESS,
    GET_NEWS_ERROR,
    GET_NEWS_REQUEST,
    GET_NEWS_RESPONSE_HEADERS,
    GET_NEWS_SUCCESS,
    POST_NEWS_ERROR,
    POST_NEWS_REQUEST,
    POST_NEWS_SUCCESS,
    PUT_NEWS_ERROR,
    PUT_NEWS_REQUEST,
    PUT_NEWS_SUCCESS
} from "./constants/News";

const API_URL = 'http://localhost:9080/news/news';

export function getNews(requestParam) {
    return (dispatch) => {
        dispatch({
            type: GET_NEWS_REQUEST
        });

        fetch(`${API_URL}/?${requestParam}`)
            .then(res => {
                if (res.ok) {
                    dispatch({
                        type: GET_NEWS_RESPONSE_HEADERS,
                        payload: res.headers.get('X-Total-Count')
                    });
                    return res.json();
                } else {
                    console.error(res.json());
                    throw new Error(`Error to load news from server. Response status code ${res.status}`)
                }
            })
            .then(result => {
                dispatch({
                    type: GET_NEWS_SUCCESS,
                    payload: result
                })
            })
            .catch(error => {
                console.error("Error to load news from server.", error);
                dispatch({
                    type: GET_NEWS_ERROR,
                    payload: "Oops! Something go wrong. Can not load news."
                })
            })
    }
}

export function getNewsById(id) {
    return (dispatch) => {
        dispatch({
            type: GET_NEWS_BY_ID_REQUEST
        });
        const request = API_URL + `/${id}`;
        fetch(request)
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    console.error(res.json());
                    throw new Error(`Error to load news with id = ${id} from server. Response status code ${res.status}`);
                }
            })
            .then(result =>
                dispatch({
                    type: GET_NEWS_BY_ID_SUCCESS,
                    payload: result
                }))
            .catch(error => {
                console.error(`Error to load news with id = ${id} from server.`, error);
                dispatch({
                    type: GET_NEWS_BY_ID_ERROR,
                    payload: "Oops! Something go wrong. Can not load news."
                })
            })
    }
}

export function saveNews(news) {
    return (dispatch) => {
        dispatch({
            type: POST_NEWS_REQUEST
        });

        fetch(API_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(news)
        })
            .then(res => {
                if (res.status === 201) {
                    return res.json();
                } else {
                    console.error(res.json());
                    throw new Error(`Error to save news. Response status code ${res.status}`);
                }
            })
            .then(result => {
                dispatch({
                    type: POST_NEWS_SUCCESS,
                    payload: {
                        result: result,
                        message: "news successful saved"
                    }
                });
                console.log(`news successful saved`);
            })
            .catch(error => {
                console.error("Error to save news.", error);
                dispatch({
                    type: POST_NEWS_ERROR,
                    payload: "Oops! Something go wrong. Can not save news."
                })
            })
    }
}

export function deleteNews(id) {
    return (dispatch) => {
        dispatch({
            type: DELETE_NEWS_BY_ID_REQUEST
        });

        fetch(API_URL + `/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then(res => {
                if (res.status === 204) {
                    dispatch({
                        type: DELETE_NEWS_BY_ID_SUCCESS,
                        payload: {
                            message: "news successful deleted",
                            id: id
                        }
                    });
                    console.log(`news with id  = ${id} successful deleted`);
                } else {
                    console.error(res.json());
                    throw new Error(`Error to delete news. Response status code ${res.status}`);
                }
            })
            .catch(error => {
                console.error("Error to delete news.", error);
                dispatch({
                    type: DELETE_NEWS_BY_ID_ERROR,
                    payload: "Oops! Something go wrong. Can not delete tag."
                })
            })
    }
}

export function updateNews(news) {
    return (dispatch) => {
        dispatch({
            type: PUT_NEWS_REQUEST
        });

        fetch(API_URL + `/${news.id}`,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(news)
            })
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    console.error(res.json());
                    throw Error(`Error to update news with id = ${news.id}. Response status code ${res.status}`);
                }
            })
            .then(result => {
                dispatch({
                    type: PUT_NEWS_SUCCESS,
                    payload: {
                        result: result,
                        message: "news successful updated"
                    }
                });
                console.log(`news with id = ${news.id} successful updated`);
            })
            .catch(error => {
                console.error("Error to update news.", error);
                dispatch({
                    type: PUT_NEWS_ERROR,
                    payload: "Oops! Something go wrong. Can not update news."
                })
            })
    }
}

export function getFoundNews(requestParam) {
    return (dispatch)=> {
        dispatch({
            type: GET_FOUND_NEWS_REQUEST
        });

        fetch(API_URL + `?${requestParam}`)
            .then(res => {
                if (res.ok) {
                    dispatch({
                        type: GET_NEWS_RESPONSE_HEADERS,
                        payload: res.headers.get('X-Total-Count')
                    });
                    return res.json();
                } else {
                    console.error(res.json());
                    throw new Error(`Error to load news from server. Response status code ${res.status}`)
                }
            })
            .then(result => {
                console.log(result);
                dispatch({
                    type: GET_FOUND_NEWS_SUCCESS,
                    payload: result
                })
            })
            .catch(error => {
                console.error("Error to load news from server.", error);
                dispatch({
                    type: GET_FOUND_NEWS_ERROR,
                    payload: "Oops! Something go wrong. Can not load news."
                })
            })
    }
}