import React from 'react';
import './App.css';
import Header from "./containers/Header";
import MainLayoutComponent from "./containers/MainLayout";
import Footer from "./containers/Footer";


class App extends React.Component {
    render() {
        return (
            <div className="App">
                <Header/>
                <MainLayoutComponent/>
                <Footer/>
            </div>
        )
    }
}

export default App
