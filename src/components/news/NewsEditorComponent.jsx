import React, { Component } from 'react';

import CreatableSelect from 'react-select/creatable';
import makeAnimated from 'react-select/animated';

const animatedComponents = makeAnimated();
export default class CreatableMulti extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: "name",
            isEdit: false,
            message: "",
            isValid: false,
            newTags:[]
        };
    }


    handleChange = (newValue, actionMeta) => {

        const newTag = newValue;
        this.setState({newTags: newTag});
        console.log(newValue);
        console.group('Value Changed');
        console.log(newValue);
        console.log(`action: ${actionMeta.action}`);
        console.groupEnd();
    };


    render() {
        const array = [{value: "tag1", label: "tag1"},{value: "tag2", label: "tag2"}];
        console.log(this.state);
        console.log(this.props);
        return (
            <CreatableSelect
                components={animatedComponents}
                isClearable
                placeholder="Input tag"
                onInputChange={this.handleChange}
                // isMulti
                onChange={this.handleChange}
                options={array}
            />
        );
    }
}
