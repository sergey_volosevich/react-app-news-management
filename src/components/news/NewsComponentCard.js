import * as React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import NewsEditButtons from "./NewsEditButtons";
import {Redirect} from "react-router";
import {withTranslation} from 'react-i18next';

class NewsComponentCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDeleted: false
        };
        this.handleDeleteNews = this.handleDeleteNews.bind(this);
    }

    componentDidMount() {
        this.props.newsActions.getNewsById(this.props.id);
    }

    handleDeleteNews() {
        this.props.newsActions.deleteNews(this.props.id);
        this.setState({isDeleted: true})
    }

    render() {
        const {fetching, message} = this.props.content;
        if (fetching) {
            return <div>{this.props.t('Loading')}</div>
        }
        const content = this.props.content.content;
        const id = this.props.id;
        const find = content.find(item => {
            return item.id === id
        });
        if (content.length === 0) {
            return null;
        }
        return (
            <React.Fragment>
                {message && <div>{message}</div>}
                {this.state.isDeleted ? <Redirect to="/"/>
                    : (
                        <div key={find.id} className="news-card">
                            <div className="news-description">
                                <h1 className="title">
                                    {find.title}
                                </h1>
                                <Link to="/" className="close">
                                    {this.props.t('MainLayout.ButtonGroup.closeButton')}
                                </Link>
                            </div>
                            <div className="news-description">
                                <p className="author">
                                    {find.author.name + " " + find.author.surname}
                                </p>
                                <p className="creationDate">
                                    {find.creationDate}
                                </p>
                            </div>
                            <p className="fullText">
                                {find.fullText}
                            </p>
                            <div className="tag-fullNews">
                                {
                                    find.tags.map(tag => "#" + tag.name + " ")
                                }
                            </div>
                            {this.props.user.user ? (
                                <div className="edit-btn-bottom">
                                <NewsEditButtons id={id} deleteAction={this.handleDeleteNews}/>
                                </div>) :
                                null
                            }
                        </div>
                    )}
            </React.Fragment>
        );
    }
}

export default withTranslation()(NewsComponentCard);

NewsComponentCard.propTypes = {
    content: PropTypes.object.isRequired,
    newsActions: PropTypes.object.isRequired,
    id: PropTypes.number.isRequired,
    user: PropTypes.object.isRequired
};