import * as React from "react";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import NewsEditButtons from "./NewsEditButtons";

class NewsComponent extends React.Component {
    constructor(props) {
        super(props);
        this.handleDeleteNews = this.handleDeleteNews.bind(this);
    }

    handleDeleteNews() {
        this.props.newsActions.deleteNews(this.props.description.id);
    }

    render() {
        const {id, title, shortText, creationDate, author, tags} = this.props.description;
        return (
            <>
                <h3 className="title">
                    <Link to={"/news/" + id}>
                        {title}
                    </Link>
                </h3>
                <div className="news-description">
                    <p className="author">
                        {author.name + " " + author.surname}
                    </p>
                    <p className="creationDate">
                        {creationDate}
                    </p>
                </div>
                <p className="shortText">
                    {shortText}
                </p>
                <div className="tag">
                    {
                        tags.map(tag => "#" + tag.name + " ")
                    }
                </div>
                {this.props.user.user ?
                    <NewsEditButtons id={id} deleteAction={this.handleDeleteNews}/> :
                    null}
            </>
        )
    }
}

NewsComponent.propTypes = {
    description: PropTypes.object.isRequired,
    newsActions: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
};

export default NewsComponent