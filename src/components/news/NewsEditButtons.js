import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {withTranslation} from 'react-i18next';

class NewsEditButtons extends React.Component {
    render() {
        const t = this.props.t;
        return (
            <React.Fragment>
                <div className="form-editor-btn-container">
                    <Link
                        to={"/editNews/"+this.props.id}
                        className="submit">
                        {t('MainLayout.ButtonGroup.editButton')}
                    </Link>
                    <button
                        className="cancel"
                        onClick={this.props.deleteAction}>
                        {t('MainLayout.ButtonGroup.deleteButton')}
                    </button>
                </div>
            </React.Fragment>
        )
    }
}

export default withTranslation()(NewsEditButtons);

NewsEditButtons.propTypes = {
    deleteAction: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired
};