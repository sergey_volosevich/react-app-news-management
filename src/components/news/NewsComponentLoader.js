import * as React from "react";
import NewsComponent from "./NewsComponent";
import PropTypes from "prop-types";
import EmptyComponent from "../EmptyComponent";
import {withTranslation} from 'react-i18next';
import {Link} from "react-router-dom";
import queryString from 'query-string';

class NewsComponentLoader extends React.Component {

    componentDidMount() {
        let search = this.props.search;
        let param = queryString.stringify(search);
        if (param) {
            this.props.newsActions.getNews(param);
            return;
        }
        param = `pag=limit:${this.props.limit};page:1;`;
        this.props.newsActions.getNews(param);
    }

    render() {
        const {fetching, content} = this.props.content;
        const t = this.props.t;
        if (fetching) {
            return <div>{t('Loading')}</div>
        }
        return (
            <>
                {content.length !== 0 ? this.renderFullNews() : this.renderEmptyNews()}
            </>
        )
    }

    renderFullNews() {
        const content = this.props.content.content;
        const t = this.props.t;
        return (
            <React.Fragment>
                <div className="result-container">
                    {t('FoundResult')}{this.props.pagination.totalCount}. {t('CurrentPage')}{this.props.currentPage}
                    <div className="pagination">
                        {this.renderRecordsPerPage()}
                    </div>
                </div>
                <ul>
                    {content.map(item =>
                        <li key={item.id} className="news">
                            <NewsComponent
                                user={this.props.user}
                                newsActions={this.props.newsActions}
                                description={item}
                            />
                        </li>
                    )}
                </ul>
                <div className="pagination">
                    {this.mapToPaginationLinks()}
                </div>
            </React.Fragment>
        )
    }

    renderEmptyNews() {
        const t = this.props.t;
        return (
            <EmptyComponent message={t('NewsEmptyMessage')}/>
        )

    }

    handleClickLimit(queryParam, newLimit) {
        this.props.newsActions.getNews(queryParam);
        this.props.handleClickLimit(newLimit);
    }

    renderRecordsPerPage() {
        let limitValues = [5, 10, 25];
        const limit = this.props.limit;
        const search = this.props.search;
        return limitValues.map(item => {
            let param;
            if (search.search) {
                param = queryString.stringify(queryString.parse(`?search=${search.search}&pag=limit:${item};page:1;`));
            } else {
                param = queryString.stringify(queryString.parse(`?pag=limit:${item};page:1;`));
            }
            if (item === limit) {
                return (<p key={item} className="active">{item}</p>)
            }
            return (<Link key={item}
                          to={`?${param}`}
                          onClick={() => this.handleClickLimit(param, item)}
            >{item}</Link>)
        });
    }

    handleClickLink(queryParam, newCurrentPage) {
        this.props.newsActions.getNews(queryParam);
        this.props.handleClickLink(newCurrentPage)
    }

    mapToPaginationLinks() {
        let {totalCount} = this.props.pagination;
        const limit = this.props.limit;
        const currentPage = this.props.currentPage;
        let numberOfPages = Math.floor(totalCount / limit);
        if (totalCount % limit !== 0) {
            numberOfPages++;
        }
        if (numberOfPages === 1) {
            return null;
        }
        let links = [];
        if (currentPage !== 1) {
            const param = this.getRequestParam(limit, currentPage - 1);
            links.push(<Link key={0}
                             onClick={() => this.handleClickLink(param, currentPage - 1)}
                             to={`?${param}`}>&laquo;</Link>)
        }
        let start = currentPage;
        let end = 10;
        if (numberOfPages <= 10) {
            start = 1;
            end = numberOfPages;
        } else {
            if (currentPage + 5 <= end) {
                start = 1;
            }
            if (currentPage + 5 > end) {
                end = currentPage + 5;
                start = currentPage - 4;
            }
            if (end >= numberOfPages) {
                end = numberOfPages;
                start = numberOfPages - 9;
            }
        }
        for (let i = start; i <= end; i++) {
            const param = this.getRequestParam(limit, i);
            if (currentPage === i) {
                links.push(<p key={currentPage} className="active">{i}</p>)
            } else {
                links.push(<Link
                    key={i}
                    to={`?${param}`}
                    onClick={() => this.handleClickLink(param, i)}
                >{i}</Link>)
            }
        }
        if (currentPage < numberOfPages) {
            const param = this.getRequestParam(limit, currentPage + 1);
            links.push(<Link key={numberOfPages + 1}
                             onClick={() => this.handleClickLink(param, currentPage + 1)}
                             to={`?${param}`}>&raquo;</Link>)
        }
        return links;
    }

    getRequestParam(limit, currentPage) {
        const search = this.props.search;
        if (search.search) {
            return queryString.stringify(queryString.parse(`?search=${search.search}&pag=limit:${limit};page:${currentPage};`));
        }
        return queryString.stringify(queryString.parse(`?pag=limit:${limit};page:${currentPage};`));
    }
}

export default withTranslation()(NewsComponentLoader)

NewsComponentLoader.propTypes = {
    content: PropTypes.object.isRequired,
    newsActions: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    pagination: PropTypes.object.isRequired,
    limit: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    handleClickLink: PropTypes.func.isRequired,
    handleClickLimit: PropTypes.func.isRequired
};