import React from "react";
import Select from "react-select";
import makeAnimated from "react-select/animated/dist/react-select.esm";
import CreatableSelect from "react-select/creatable/dist/react-select.esm";
import ValidationMsgComponent from "../ValidationMsgComponent";
import PropTypes from "prop-types"
import {Link} from "react-router-dom";
import {withTranslation} from "react-i18next";

const animatedComponentsTag = makeAnimated();
const initState = {
    title: "",
    shortText: "",
    fullText: "",
    authorName: "",
    authorSurname: "",
    author: null,
    tags: [],
    formErrors: {
        title: "",
        shortText: "",
        fullText: "",
        authorName: "",
        authorSurname: "",
        tag: ""
    },
    isTitleValid: false,
    isShortTextValid: false,
    isFullTextValid: false,
    isNameValid: false,
    isSurnameValid: false,
    isFormValid: false,
    isTagValid: false,
    isAddAuthor: false
};

class NewsFormComponent extends React.Component {
    constructor(props) {
        super(props);
        this.onsubmit = this.onsubmit.bind(this);
        this.state = initState;
        this.handleClickAddAuthor = this.handleClickAddAuthor.bind(this);
        this.handleClickDisableAddAuthor = this.handleClickDisableAddAuthor.bind(this);
        this.onHandleChangeNewsInput = this.onHandleChangeNewsInput.bind(this);
    }

    handleClickAddAuthor(e) {
        e.preventDefault();
        this.setState({
            isAddAuthor: true
        })
    }

    handleClickDisableAddAuthor(e) {
        e.preventDefault();
        this.setState({
            isAddAuthor: false,
            authorName: "",
            authorSurname: "",
            isNameValid: false,
            isSurnameValid: false,
            isFormValid: false,
        })
    }

    onHandleChangeNewsInput(e) {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        }, () => {
            this.validate(name, value)
        });
    }

    validate(fieldName, fieldValue) {
        const t = this.props.t;
        let fieldValidationErrors = this.state.formErrors;
        let isTitleValid = this.state.isTitleValid;
        let isShortTextValid = this.state.isShortTextValid;
        let isFullTextValid = this.state.isFullTextValid;
        let isNameValid = this.state.isNameValid;
        let isSurnameValid = this.state.isSurnameValid;
        let match = new RegExp("^[а-яА-ЯёЁa-zA-Z-]{3,30}$");
        switch (fieldName) {
            case 'title':
                isTitleValid = fieldValue.length >= 10 && fieldValue.length <= 30;
                fieldValidationErrors.title = isTitleValid ? "" : t('MainLayout.NewsRedactor.titleValidationMsg');
                break;
            case "shortText":
                isShortTextValid = fieldValue.length >= 20 && fieldValue.length <= 100;
                fieldValidationErrors.shortText = isShortTextValid ? "" : t('MainLayout.NewsRedactor.shortTextValidationMsg');
                break;
            case "fullText":
                isFullTextValid = fieldValue.length >= 50 && fieldValue.length <= 2000;
                fieldValidationErrors.fullText = isFullTextValid ? "" : t('MainLayout.NewsRedactor.fullTextValidationMsg');
                break;
            case 'authorName':
                isNameValid = match.test(fieldValue);
                fieldValidationErrors.authorName = isNameValid ? "" : t('MainLayout.AuthorRedactor.nameValidationMsg');
                break;
            case "authorSurname":
                isSurnameValid = match.test(fieldValue);
                fieldValidationErrors.authorSurname = isSurnameValid ? "" : t('MainLayout.AuthorRedactor.surnameValidationMsg');
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            isTitleValid: isTitleValid,
            isShortTextValid: isShortTextValid,
            isFullTextValid: isFullTextValid,
            isNameValid: isNameValid,
            isSurnameValid: isSurnameValid
        }, () => this.validateForm());
    };

    validateForm() {
        let isTitleValid = this.state.isTitleValid;
        let isShortTextValid = this.state.isShortTextValid;
        let isFullTextValid = this.state.isFullTextValid;
        let isNameValid = this.state.isNameValid;
        let isSurnameValid = this.state.isSurnameValid;
        let isTagValid = this.state.isTagValid;
        this.setState({
            isFormValid: isTitleValid && isShortTextValid && isFullTextValid && isNameValid && isSurnameValid
                && isTagValid
        });
    };


    handleSelectChangeAuthor = (newValue, actionMeta) => {
        if (actionMeta.action === 'select-option') {
            this.setState({
                author: newValue,
                isNameValid: true,
                isSurnameValid: true,
            }, () => this.validateForm());
        } else {
            this.setState({
                author: newValue,
                isNameValid: false,
                isSurnameValid: false,
            }, () => this.validateForm());
        }
        console.log(newValue);
        console.group('Value Changed');
        console.log(newValue);
        console.log(`action: ${actionMeta.action}`);
        console.groupEnd();
    };

    handleSelectChangeTag = (newValue, actionMeta) => {
        const actionName = actionMeta.action;
        this.setState({tags: newValue});
        if (actionName === 'select-option') {
            this.setState({
                isTagValid: true
            }, () => this.validateForm());
        } else if (!newValue || actionName === 'clear') {
            this.setState({
                isTagValid: false
            }, () => this.validateForm());
        } else if (actionName === 'create-option') {
            this.setState({
                isTagValid: true
            }, () => this.validateForm());
        }
        console.group('Value Changed');
        console.log(newValue);
        console.log(`action: ${actionName}`);
        console.groupEnd();
    };

    handleTagInput = (value) => {
        this.validateTag(value);
    };

    validateTag(value) {
        if (!value) {
            return;
        }
        const t = this.props.t;
        const fieldValidationErrors = this.state.formErrors;
        this.setState({name: value});
        let match = new RegExp("^[а-яА-ЯёЁa-zA-Z0-9_]{3,27}$");
        if (match.test(value)) {
            fieldValidationErrors.tag = "";
            this.setState({
                isTagValid: true,
                formErrors: fieldValidationErrors
            });
        } else {
            fieldValidationErrors.tag = t('MainLayout.TagRedactor.tagValidationMsg');
            this.setState({
                isTagValid: false,
                formErrors: fieldValidationErrors
            });
        }
    };

    onsubmit(e) {
        e.preventDefault();
        let author = this.prepareToSaveAuthor();
        const tags = this.prepareTagsToSave();
        const date = this.createDate();
        let news = {
            title: this.state.title,
            shortText: this.state.shortText,
            fullText: this.state.fullText,
            creationDate: date,
            modificationDate: date,
            author: author,
            tags: tags
        };
        this.props.saveNews(news);
        this.setState(initState);
    }

    createDate() {
        const date = new Date();
        let mm = date.getMonth() + 1;
        let dd = date.getDate();
        let yyyy = date.getFullYear();
        if (mm < 10) {
            mm = '0' + mm;
        }
        return `${yyyy}-${mm}-${dd}`;
    }

    prepareToSaveAuthor() {
        let author = {};
        if (this.state.authorName && this.state.authorSurname) {
            author = {
                name: this.state.authorName,
                surname: this.state.authorSurname
            };
        } else {
            const item = this.state.author.item;
            author = {
                id: item.id,
                name: item.name,
                surname: item.surname
            };
        }
        return author;
    }

    prepareTagsToSave() {
        return this.state.tags.map(item => {
            if (item.__isNew__) {
                return {name: item.value};
            }
            return {
                id: item.item.id,
                name: item.item.name
            }
        });
    }

    render() {
        const t = this.props.t;
        return (
            <div className="wrapper">
                <form className="news-form-editor" onSubmit={this.onsubmit}>
                    <h1>{t('MainLayout.NewsRedactor.headerAdd')}</h1>
                    <label htmlFor="title">{t('MainLayout.NewsRedactor.titleLabel')}</label>
                    <div className="input-wrapper">
                        <input name="title" id="title"
                               type="text" value={this.state.title}
                               placeholder={t('MainLayout.NewsRedactor.titlePlaceHolder')}
                               onChange={this.onHandleChangeNewsInput}/>
                        <ValidationMsgComponent validationMessage={this.state.formErrors.title}/>
                    </div>
                    <label htmlFor="shortText">{t('MainLayout.NewsRedactor.shortTextLabel')}</label>
                    <div className="input-wrapper">
                        <textarea name="shortText" id="shortText"
                                  placeholder={t('MainLayout.NewsRedactor.shortTextPlaceholder')}
                                  value={this.state.shortText}
                                  onChange={this.onHandleChangeNewsInput}/>
                        <ValidationMsgComponent validationMessage={this.state.formErrors.shortText}/>
                    </div>
                    <label htmlFor="fullText">{t('MainLayout.NewsRedactor.fullTextLabel')}</label>
                    <div className="input-wrapper">
                         <textarea name="fullText" id="fullText"
                                   placeholder={t('MainLayout.NewsRedactor.fullTextPlaceholder')}
                                   value={this.state.fullText}
                                   onChange={this.onHandleChangeNewsInput}/>
                        <ValidationMsgComponent validationMessage={this.state.formErrors.fullText}/>
                    </div>
                    {this.renderAuthorBlockTemplate()}
                    {this.renderTagBlockTemplate()}
                    <div className="btn-container">
                        <button className={!this.state.isFormValid ? "submit disabled" : "submit"}
                                disabled={!this.state.isFormValid}>
                            {t('MainLayout.ButtonGroup.addButton')}
                        </button>
                        <Link to="/" className="cancel">{t('MainLayout.Login.cancelButton')}</Link>
                    </div>
                </form>
            </div>
        )
    }

    renderAuthorBlockTemplate() {
        const t = this.props.t;
        const {fetching, content, message} = this.props.authors;
        if (fetching) {
            return <div>{t('Loading')}</div>
        }
        const authors = content.map(author => {
            let nameAndSurname = author.name + " " + author.surname;
            return {value: author.id, label: nameAndSurname, item: author}
        });

        if (this.state.isAddAuthor) {
            return (
                <React.Fragment>
                    <label>{t('MainLayout.AuthorRedactor.addAuthorLabel')}</label>
                    <div className="input-wrapper" style={{paddingBottom: "10px"}}>
                        <input name="authorName"
                               placeholder={t('MainLayout.AuthorRedactor.authorPlaceHolderName')}
                               value={this.state.authorName}
                               onChange={this.onHandleChangeNewsInput}/>
                        <ValidationMsgComponent validationMessage={this.state.formErrors.authorName}/>
                    </div>
                    <div className="input-wrapper">
                        <input name="authorSurname"
                               placeholder={t('MainLayout.AuthorRedactor.authorPlaceHolderSurname')}
                               value={this.state.authorSurname}
                               onChange={this.onHandleChangeNewsInput}/>
                        <ValidationMsgComponent validationMessage={this.state.formErrors.authorSurname}/>
                    </div>
                    <div className='btn-container' style={{paddingBottom: "10px"}}>
                        <button type="button" className="cancel" onClick={this.handleClickDisableAddAuthor}>
                            {t('MainLayout.ButtonGroup.discardButton')}
                        </button>
                    </div>
                </React.Fragment>
            )
        } else {
            return (
                <React.Fragment>
                    {message && <div>{message}</div>}
                    <div className='btn-container' style={{paddingBottom: "10px"}}>
                        <button className="submit" onClick={this.handleClickAddAuthor}>
                            {t('MainLayout.ButtonGroup.createAuthorBtn')}
                        </button>
                    </div>
                    <div className="input-wrapper" style={{paddingBottom: "10px"}}>
                        <Select
                            noOptionsMessage={() => t('MainLayout.NewsRedactor.authorOptions')}
                            value={this.state.author}
                            escapeClearsValue="true"
                            isClearable={true}
                            isSearchable={true}
                            placeholder={t('MainLayout.NewsRedactor.selectAuthor')}
                            onChange={this.handleSelectChangeAuthor}
                            options={authors}
                        />
                        <ValidationMsgComponent validationMessage={this.state.formErrors.authorName}/>
                        <ValidationMsgComponent validationMessage={this.state.formErrors.authorSurname}/>
                    </div>
                </React.Fragment>
            )
        }
    }

    renderTagBlockTemplate() {
        const t = this.props.t;
        const {fetching, content, message} = this.props.tags;
        if (fetching) {
            return <div>{t('Loading')}</div>
        }
        const tags = content.map(tag => {
            return {value: tag.id, label: tag.name, item: tag}
        });
        return (
            <div className="input-wrapper">
                {message && <div>{message}</div>}
                <CreatableSelect
                    value={this.state.tags}
                    components={animatedComponentsTag}
                    isClearable
                    noOptionsMessage={() => t('MainLayout.NewsRedactor.tagNoOptions')}
                    placeholder={t('MainLayout.NewsRedactor.selectTags')}
                    isValidNewOption={(inputValue, selectValue, selectOptions) => {
                        if (inputValue.trim().length === 0 || selectOptions.find(option => option.name === inputValue)) {
                            return false;
                        }
                        return this.state.isTagValid;
                    }}
                    onInputChange={this.handleTagInput}
                    isMulti
                    onChange={this.handleSelectChangeTag}
                    options={tags}
                />
                <ValidationMsgComponent validationMessage={this.state.formErrors.tag}/>
            </div>
        );
    }
}

export default withTranslation()(NewsFormComponent);

NewsFormComponent.propTypes = {
    tagActions: PropTypes.object.isRequired,
    authorActions: PropTypes.object.isRequired,
    tags: PropTypes.object.isRequired,
    authors: PropTypes.object.isRequired,
    saveNews: PropTypes.func.isRequired
};