import React from "react";
import TagSearchComponent from "../tag/TagSearchComponent";
import AuthorSearchComponent from "../author/AuthorSearchComponent";
import PropTypes from "prop-types";
import {withTranslation} from 'react-i18next';
import queryString from 'query-string';

class SearchComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedTags: null,
            selectedAuthor: null
        }
    }

    handleSelectChangeTag = (newValue) => {
        this.setState({selectedTags: newValue});
    };

    handleSelectChangeAuthor = (newValue) => {
        this.setState({
            selectedAuthor: newValue
        });
    };

    onClickReset() {
        this.setState({
            selectedTags: [],
            selectedAuthor: null
        });
        this.props.history.push({
            pathname: '/',
            search: null
        });
    }

    onClickSearch() {
        const author = this.state.selectedAuthor;
        const tags = this.state.selectedTags;
        const {limit, history} = this.props;
        let authorParameter;
        const pag = `&pag=limit:${limit};page:1;`;
        let tagParameter = "";
        if (author) {
            const {name, surname} = author.item;
            authorParameter = `authorName:${name};authorSurname:${surname};`;
        }
        if (tags) {
            let tagNames = tags.map(tag => tag.item.name);
            for (const tagName of tagNames) {
                let tag = `tag:${tagName};`;
                tagParameter += tag;
            }
        }

        if (authorParameter && tagParameter) {
            let requestParam = `?search=${authorParameter}${tagParameter}${pag}`;
            parseRequestParam.call(this, requestParam);
        } else if (authorParameter) {
            let requestParam = `?search=${authorParameter}${pag}`;
            parseRequestParam.call(this, requestParam);
        } else if (tagParameter) {
            let requestParam = `?search=${tagParameter}${pag}`;
            parseRequestParam.call(this, requestParam);
        }

        function parseRequestParam(requestParam) {
            let parsedRequestParam = queryString.stringify(queryString.parse(requestParam));
            history.push({
                pathname: '/',
                search: requestParam
            });
            this.props.newsActions.getFoundNews(parsedRequestParam);
        }

        this.props.setCurrentPage(1);
    }

    render() {
        const {tags, authors} = this.props;
        const t = this.props.t;
        return (
            <div className="search-form">
                <div className="search-form-select-container">
                    <TagSearchComponent content={tags}
                                        selectedTags={this.state.selectedTags}
                                        tagActions={this.props.tagActions}
                                        changeTag={this.handleSelectChangeTag}/>
                    <AuthorSearchComponent content={authors}
                                           selectedAuthor={this.state.selectedAuthor}
                                           changeAuthor={this.handleSelectChangeAuthor}
                                           authorActions={this.props.authorActions}/>
                </div>
                <div className="search-form-btn-container">
                    <button className='submit'
                            onClick={() => {
                                this.onClickSearch()
                            }}>
                        {t('MainLayout.searchButton')}
                    </button>
                    <button className='cancel'
                            onClick={() => {
                                this.onClickReset()
                            }}>
                        {t('MainLayout.resetButton')}
                    </button>
                </div>
            </div>
        );
    }
}

export default withTranslation()(SearchComponent);

SearchComponent.propTypes = {
    tags: PropTypes.object.isRequired,
    authors: PropTypes.object.isRequired,
    tagActions: PropTypes.object.isRequired,
    authorActions: PropTypes.object.isRequired,
    newsActions: PropTypes.object.isRequired,
    limit: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
};