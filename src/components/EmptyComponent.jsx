import React from "react";
import PropTypes from "prop-types";

export default class EmptyComponent extends React.Component{
    render() {
        return(
            <div className="empty-msg-container">
                {this.props.message}
            </div>
        )
    }
}

EmptyComponent.propTypes = {
  message: PropTypes.string.isRequired
};