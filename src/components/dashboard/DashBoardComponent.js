import * as React from "react";
import {NavLink} from "react-router-dom";
import {withTranslation} from 'react-i18next';

class DashBoardComponent extends React.Component {
    render() {
        const t = this.props.t;
        return (
            <div className="dashboard-container">
                <h4>{t('MainLayout.Dashboard.header')}</h4>
                <NavLink to="/addAuthor" activeClassName="selected" className="dashboard">
                    {t('MainLayout.Dashboard.author')}
                </NavLink>
                <NavLink to="/addNews" activeClassName="selected" className="dashboard">
                    {t('MainLayout.Dashboard.news')}
                </NavLink>
                <NavLink to="/addTag" activeClassName="selected" className="dashboard">
                    {t('MainLayout.Dashboard.tag')}
                </NavLink>
            </div>
        )
    }
}

export default withTranslation()(DashBoardComponent);