import React from "react";
import {Link} from "react-router-dom";
import {withTranslation} from 'react-i18next';

class PageNotFoundComponent extends React.Component {
    render() {
        const t = this.props.t;
        return (
            <div className="error-page-container">
                <h1 className="error-header">404</h1>
                <div className="error-message">{t('404message')}</div>
                <hr/>

                <Link to="/" className="submit">{t('MainLayout.ButtonGroup.backButton')}</Link>
            </div>
        )
    }
}

export default withTranslation()(PageNotFoundComponent)