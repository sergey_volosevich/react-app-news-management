import React from "react";
import PropTypes from "prop-types"
import {withTranslation} from 'react-i18next';

class TagEditorComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.tag.name,
            isEdit: false,
            message: "",
            isValid: false
        };
        this.onDeleteClick = this.onDeleteClick.bind(this);
        this.onEditClick = this.onEditClick.bind(this);
        this.onDiscardClick = this.onDiscardClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        let tag = {
            id: this.props.tag.id,
            name: this.state.name
        };
        if (this.state.isValid) {
            this.props.tagActions.updateTag(tag);
            this.setState({isEdit: false})
        }
    }

    onDeleteClick(e) {
        e.preventDefault();
        this.props.tagActions.deleteTag(this.props.tag.id);
    }

    onEditClick(e) {
        e.preventDefault();
        this.setState({
            isEdit: true,
            isValid: true
        });
    }

    onDiscardClick(e) {
        e.preventDefault();
        this.setState({
            name: this.props.tag.name,
            isEdit: false,
            message: ""
        });
    }

    handleInputChange(e) {
        e.preventDefault();
        this.validate(e.target.value);
    }

    validate = (value) => {
        const t = this.props.t;
        this.setState({name: value});
        let match = new RegExp("^[а-яА-ЯёЁa-zA-Z0-9_]{3,27}$");
        if (match.test(value)) {
            this.setState({
                isValid: true,
                message: ""
            });
        } else {
            this.setState({
                isValid: false,
                message: t('MainLayout.TagRedactor.tagValidationMsg')
            })
        }
    };

    render() {
        const t = this.props.t;
        return (
            <div className="wrapper">
                <form className="form-editor" onSubmit={this.onSubmit}>
                    <label >{t('MainLayout.TagRedactor.editTagLabel')}</label>
                    <div className="from-editor-input-inline">
                        <input value={this.state.name} disabled={!this.state.isEdit} onChange={this.handleInputChange}/>
                    </div>
                    {this.renderTemplate()}
                </form>
            </div>
        )
    }

    //TODO remove styles from code to the file
    renderTemplate = () => {
        const t = this.props.t;
        if (this.state.isEdit) {
            return (
                <React.Fragment>
                    <div className="form-editor-btn-container">
                    <button type="submit" disabled={!this.state.isValid} className="submit">
                        {t('MainLayout.ButtonGroup.saveButton')}
                    </button>
                    <button className="cancel" onClick={this.onDiscardClick}>
                        {t('MainLayout.ButtonGroup.discardButton')}
                    </button>
                    </div>
                </React.Fragment>
            )
        }
        return (
            <React.Fragment>
                <div className="form-editor-btn-container">
                    <button className="submit"  onClick={this.onEditClick}>
                        {t('MainLayout.ButtonGroup.editButton')}
                    </button>
                    <button className="cancel"  onClick={this.onDeleteClick}>
                        {t('MainLayout.ButtonGroup.deleteButton')}
                    </button>
                </div>
            </React.Fragment>
        )

    }
}

export default withTranslation()(TagEditorComponent)

TagEditorComponent.propTypes = {
    tag: PropTypes.object.isRequired,
    tagActions: PropTypes.object.isRequired,
};