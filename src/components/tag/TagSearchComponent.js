import * as React from "react";
import Select from "react-select";
import PropTypes from "prop-types";
import {withTranslation} from 'react-i18next';
import makeAnimated from "react-select/animated/dist/react-select.esm";

const animatedComponentsTag = makeAnimated();
class TagSearchComponent extends React.Component {

    componentDidMount() {
        this.props.tagActions.getTags();
    }

    handleSelectChangeTag = (newValue, actionMeta) => {
        this.props.changeTag(newValue, actionMeta);
    };

    render() {
        const {fetching, content} = this.props.content;
        const t = this.props.t;
        const tags = content.map(tag => {
                return {value: tag.name, label: tag.name, item: tag}
            }
        );
        return (
            <Select
                value={this.props.selectedTags}
                closeMenuOnSelect={false}
                components={animatedComponentsTag}
                options={tags}
                isMulti
                noOptionsMessage={() => t('MainLayout.Search.noTags')}
                onChange={this.handleSelectChangeTag}
                placeholder={t('MainLayout.Search.tagPlaceHolder')}/>
        )
    }
}

export default withTranslation()(TagSearchComponent);

TagSearchComponent.propTypes = {
    content: PropTypes.object.isRequired,
    tagActions: PropTypes.object.isRequired
};