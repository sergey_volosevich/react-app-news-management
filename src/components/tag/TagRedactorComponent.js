import React from "react";
import PropTypes from "prop-types"
import TagEditorComponent from "./TagEditorComponent";
import TagFormComponent from "./TagFormComponent";
import EmptyComponent from "../EmptyComponent";
import {withTranslation} from 'react-i18next';

class TagRedactorComponent extends React.Component {
    componentDidMount() {
        this.props.tagActions.getTags();
    }

    render() {
        const {fetching, content, message} = this.props.tags;
        const t = this.props.t;
        if (fetching) {
            return <div>{t('Loading')}</div>
        }
        //TODO add css to div error
        return (
            <React.Fragment>
                <h1>{t('MainLayout.TagRedactor.header')}</h1>
                <TagFormComponent tagActions={this.props.tagActions}/>
                {message && <div>{message}</div>}
                {content.length!==0 ? content.map(tag =>
                    <React.Fragment key={tag.id}>
                        <TagEditorComponent tag={tag} tagActions={this.props.tagActions}/>
                    </React.Fragment>
                ) : <EmptyComponent message={t('TagEmptyMessage')}/>}
            </React.Fragment>
        )
    }
}

export default withTranslation()(TagRedactorComponent)

TagRedactorComponent.propTypes = {
    tags: PropTypes.object.isRequired,
    tagActions: PropTypes.object.isRequired,
};