import React from "react";
import PropTypes from "prop-types";
import ValidationMsgComponent from "../ValidationMsgComponent";
import {withTranslation} from 'react-i18next';

class TagFormComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            message: "",
            isValid: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        let tag = {
            name: this.state.name
        };
        if (this.state.isValid) {
            this.props.tagActions.saveTag(tag);
            this.setState({
                name: "",
                message: "",
                isValid: false
            })
        }
    }

    handleInputChange(e) {
        e.preventDefault();
        this.validate(e.target.value);
    }

    validate = (value) => {
        const t = this.props.t;
        this.setState({name: value});
        let match = new RegExp("^[а-яА-ЯёЁa-zA-Z0-9_]{3,27}$");
        if (match.test(value)) {
            this.setState({
                isValid: true,
                message: ""
            });
        } else {
            this.setState({
                isValid: false,
                message: t('MainLayout.TagRedactor.tagValidationMsg')
            })
        }
    };

    //TODO remove styles from code to the file
    render() {
        const t = this.props.t;
        return (
            <div className="wrapper">
                <form className="form-editor" onSubmit={this.onSubmit}>
                    <label htmlFor="tag">{t('MainLayout.TagRedactor.addTagLabel')}</label>
                    <div className="from-editor-input-inline">
                        <input id="tag"
                               placeholder={t('MainLayout.TagRedactor.tagPlaceHolder')}
                               value={this.state.name}
                               onChange={this.handleInputChange}/>
                        <ValidationMsgComponent validationMessage={this.state.message}/>
                    </div>
                    <div className="form-editor-btn-container">
                        <button type="submit" disabled={!this.state.isValid}
                                className={!this.state.isValid ? "submit disabled" : "submit"}>
                            {t('MainLayout.ButtonGroup.addButton')}
                        </button>
                    </div>
                </form>
            </div>

        )
    }
}

export default withTranslation()(TagFormComponent);

TagFormComponent.propTypes = {
    tagActions: PropTypes.object.isRequired,
};
