import React from "react";
import PropTypes from "prop-types"
import ValidationMsgComponent from "../ValidationMsgComponent";
import {withTranslation} from 'react-i18next';

class AuthorEditorComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.author.name,
            surname: this.props.author.surname,
            isEdit: false,
            formErrors: {
                name: "",
                surname: ""
            },
            isNameValid: false,
            isSurnameValid: false,
            isValid: false
        };
        this.onEditClick = this.onEditClick.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onDiscardClick = this.onDiscardClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onDeleteClick = this.onDeleteClick.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        let author = {
            id: this.props.author.id,
            name: this.state.name,
            surname: this.state.surname
        };
        if (this.state.isValid) {
            console.log(author);
            this.props.authorActions.updateAuthor(author);
            this.setState({
                formErrors: {
                    name: "",
                    surname: ""
                },
                isEdit: false,
                isNameValid: false,
                isSurnameValid: false,
                isValid: false
            })
        }
    }

    handleInputChange(e) {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        }, () => {
            this.validate(name, value)
        });
    }

    validate(fieldName, fieldValue) {
        let fieldValidationErrors = this.state.formErrors;
        let isNameValid = this.state.isNameValid;
        let isSurnameValid = this.state.isSurnameValid;
        const t = this.props.t;
        switch (fieldName) {
            case 'name':
                isNameValid = fieldValue.match("^[а-яА-ЯёЁa-zA-Z-]{3,30}$");
                fieldValidationErrors.name = isNameValid ? "" : t('MainLayout.AuthorRedactor.nameValidationMsg');
                break;
            case "surname":
                isSurnameValid = fieldValue.match("^[а-яА-ЯёЁa-zA-Z-]{3,30}$");
                fieldValidationErrors.surname = isSurnameValid ? "" : t('MainLayout.AuthorRedactor.surnameValidationMsg');
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            isNameValid: isNameValid,
            isSurnameValid: isSurnameValid
        }, () => this.validateForm());
    };

    validateForm() {
        this.setState({
            isValid: this.state.isNameValid && this.state.isSurnameValid
        });
    };

    onDeleteClick(e) {
        e.preventDefault();
        this.props.authorActions.deleteAuthor(this.props.author.id);
    }

    onEditClick(e) {
        e.preventDefault();
        this.setState({
            isEdit: true,
            isValid: true
        });
    }

    onDiscardClick(e) {
        e.preventDefault();
        this.setState({
            name: this.props.author.name,
            surname: this.props.author.surname,
            isEdit: false,
            formErrors: {
                name: "",
                surname: ""
            },
            isValid: false
        });
    }

    render() {
        const t = this.props.t;
        return (
            <div className="wrapper">
                <form className="form-editor" onSubmit={this.onSubmit}>
                    <label>
                        {t('MainLayout.AuthorRedactor.editAuthorLabel')}
                    </label>
                    <div className="from-editor-input-inline">
                        <input disabled={!this.state.isEdit}
                               name="name" value={this.state.name}
                               onChange={this.handleInputChange}/>
                        <ValidationMsgComponent validationMessage={this.state.formErrors.name}/>
                    </div>
                    <div className="from-editor-input-inline">
                        <input disabled={!this.state.isEdit} name="surname" value={this.state.surname}
                               onChange={this.handleInputChange}/>
                        <ValidationMsgComponent validationMessage={this.state.formErrors.surname}/>
                    </div>
                    {this.renderTemplate()}
                </form>
            </div>
        )
    }

    //TODO remove styles from code to the file
    renderTemplate = () => {
        const t = this.props.t;
        if (this.state.isEdit) {
            return (
                <React.Fragment>
                    <div className="form-editor-btn-container">
                        <button type="submit" disabled={!this.state.isValid}
                                className={!this.state.isFormValid ? "submit disabled" : "submit"}>
                            {t('MainLayout.ButtonGroup.saveButton')}
                        </button>
                        <button className="cancel" onClick={this.onDiscardClick}>
                            {t('MainLayout.ButtonGroup.discardButton')}
                        </button>
                    </div>
                </React.Fragment>
            )
        }
        return (
            <React.Fragment>
                <div className="form-editor-btn-container">
                    <button className="submit" onClick={this.onEditClick}>
                        {t('MainLayout.ButtonGroup.editButton')}
                    </button>
                    <button className="cancel" onClick={this.onDeleteClick}>
                        {t('MainLayout.ButtonGroup.deleteButton')}
                    </button>
                </div>
            </React.Fragment>
        )

    }
}

export default withTranslation()(AuthorEditorComponent)

AuthorEditorComponent.propTypes = {
    author: PropTypes.object.isRequired,
    authorActions: PropTypes.object.isRequired
};