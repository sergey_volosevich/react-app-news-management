import React from "react";
import PropTypes from "prop-types"
import ValidationMsgComponent from "../ValidationMsgComponent";
import {withTranslation} from 'react-i18next';

class AuthorFormComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            surname: "",
            formErrors: {
                name: "",
                surname: ""
            },
            isNameValid: false,
            isSurnameValid: false,
            isValid: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        let author = {
            name: this.state.name,
            surname: this.state.surname
        };
        if (this.state.isValid) {

            console.log(author);
            this.props.authorActions.saveAuthor(author);
            this.setState({
                name: "",
                surname: "",
                formErrors: {
                    name: "",
                    surname: ""
                },
                isNameValid: false,
                isSurnameValid: false,
                isValid: false
            })
        }
    }

    handleInputChange(e) {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        }, () => {
            this.validate(name, value)
        });
    }

    validate(fieldName, fieldValue) {
        let fieldValidationErrors = this.state.formErrors;
        let isNameValid = this.state.isNameValid;
        let isSurnameValid = this.state.isSurnameValid;
        const t = this.props.t;
        switch (fieldName) {
            case 'name':
                isNameValid = fieldValue.match("^[а-яА-ЯёЁa-zA-Z-]{3,30}$");
                fieldValidationErrors.name = isNameValid ? "" : t('MainLayout.AuthorRedactor.nameValidationMsg');
                break;
            case "surname":
                isSurnameValid = fieldValue.match("^[а-яА-ЯёЁa-zA-Z-]{3,30}$");
                fieldValidationErrors.surname = isSurnameValid ? "" : t('MainLayout.AuthorRedactor.surnameValidationMsg');
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            isNameValid: isNameValid,
            isSurnameValid: isSurnameValid
        }, () => this.validateForm());
    };

    validateForm() {
        this.setState({
            isValid: this.state.isNameValid && this.state.isSurnameValid
        });
    };

    //TODO remove styles from code to the file
    render() {
        const t = this.props.t;
        return (
            <div className="wrapper">
                <form className="form-editor" onSubmit={this.onSubmit}>
                    <label>{t('MainLayout.AuthorRedactor.addAuthorLabel')}</label>
                    <div className="from-editor-input-inline">
                        <input name="name"
                               placeholder={t('MainLayout.AuthorRedactor.authorPlaceHolderName')}
                               value={this.state.name}
                               onChange={this.handleInputChange}/>
                        <ValidationMsgComponent validationMessage={this.state.formErrors.name}/>
                    </div>
                    <div className="from-editor-input-inline">
                        <input name="surname"
                               placeholder={t('MainLayout.AuthorRedactor.authorPlaceHolderSurname')}
                               value={this.state.surname}
                               onChange={this.handleInputChange}/>
                        <ValidationMsgComponent validationMessage={this.state.formErrors.surname}/>
                    </div>
                    <div className="form-editor-btn-container">
                        <button className={!this.state.isValid ? "submit disabled" : "submit"}
                                type="submit" disabled={!this.state.isValid}>
                            {t('MainLayout.ButtonGroup.addButton')}
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}

export default withTranslation()(AuthorFormComponent)

AuthorFormComponent.propTypes = {
    authorActions: PropTypes.object.isRequired
};
