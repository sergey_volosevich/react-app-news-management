import React from "react";
import EmptyComponent from "../EmptyComponent";
import AuthorFormComponent from "./AuthorFormComponent";
import PropTypes from "prop-types"
import AuthorEditorComponent from "./AuthorEditorComponent";
import {withTranslation} from 'react-i18next';

class AuthorRedactorComponent extends React.Component {
    componentDidMount() {
        this.props.authorActions.getAuthors();
    }

    render() {
        const {fetching, content, message} = this.props.authors;
        const t = this.props.t;
        if (fetching) {
            return <div>{t('Loading')}</div>
        }
        //TODO add css to div error
        const authorActions = this.props.authorActions;
        return (
            <div>
                <h1>{t('MainLayout.AuthorRedactor.header')}</h1>
                <AuthorFormComponent authorActions={authorActions}/>
                {message && <div>{message}</div>}
                {content.length !== 0 ? content.map(author =>
                    <div key={author.id}>
                        <AuthorEditorComponent author={author} authorActions={authorActions}/>
                    </div>
                ) : <EmptyComponent message={t('AuthorEmptyMessage')}/>}
            </div>
        )
    }
}

export default withTranslation()(AuthorRedactorComponent)

AuthorRedactorComponent.propTypes = {
    authorActions: PropTypes.object.isRequired,
    authors: PropTypes.object.isRequired
};