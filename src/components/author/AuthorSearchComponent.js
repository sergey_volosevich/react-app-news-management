import React from "react";
import Select from "react-select";
import PropTypes from "prop-types";
import {withTranslation} from 'react-i18next';

class AuthorSearchComponent extends React.Component {

    componentDidMount() {
        this.props.authorActions.getAuthors();
    }

    handleSelectChangeAuthor = (newValue, actionMeta) => {
        this.props.changeAuthor(newValue, actionMeta)
    };

    render() {
        const t = this.props.t;
        const {fetching, content} = this.props.content;
        const authors = content.map(author => {
            let authorInitials = "" + author.name + " " + author.surname;
            return {
                value: authorInitials,
                label: authorInitials,
                item: author
            }
        });
        return (
            <Select
                value={this.props.selectedAuthor}
                options={authors}
                isClearable={true}
                isSearchable={true}
                onChange={this.handleSelectChangeAuthor}
                noOptionsMessage={() => t('MainLayout.Search.noAuthors')}
                placeholder={t('MainLayout.Search.authorPlaceHolder')}/>
        )
    }
}

export default withTranslation()(AuthorSearchComponent);

AuthorSearchComponent.propTypes = {
    content: PropTypes.object.isRequired,
    authorActions: PropTypes.object.isRequired,
    changeAuthor: PropTypes.func.isRequired
};