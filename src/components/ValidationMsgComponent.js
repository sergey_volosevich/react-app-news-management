import React from "react";
import PropTypes from "prop-types"

export default class ValidationMsgComponent extends React.Component{
    render() {
        return(
            <React.Fragment>
                <small className="validation-msg">
                    {this.props.validationMessage}
                </small>
            </React.Fragment>
        )
    }
}

ValidationMsgComponent.propTypes = {
  validationMessage: PropTypes.string.isRequired
};