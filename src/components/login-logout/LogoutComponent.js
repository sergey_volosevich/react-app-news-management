import React from "react";
import {Link} from "react-router-dom";
import {withTranslation} from 'react-i18next';

class LogoutComponent extends React.Component {
    render() {
        const t = this.props.t;
        return (
            <div className="logout-page-container">
                <h1>{t('LogoutMsg')}</h1>
                <hr/>
                <Link className="submit" to="/">{t('MainLayout.ButtonGroup.backButton')}</Link>
            </div>
        )
    }
};

export default withTranslation()(LogoutComponent);