import React from "react";
import {Link, Redirect} from "react-router-dom";
import iconPassword from "../../assets/passwordCheck.png"
import PropTypes from "prop-types"
import {withTranslation} from 'react-i18next';

class LoginFormComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            isVisible: false,
            redirectToReferrer: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleIconClick = this.handleIconClick.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        const user = {
            username: this.state.username,
            password: this.state.password,
        };
        this.props.authActions.login(user);
        this.setState({
            redirectToReferrer: true
        })
    }

    handleInputChange(e) {
        const name = e.target.id;
        const value = e.target.value;
        this.setState({
            [name]: value
        });
    }

    handleIconClick() {
        this.setState({
            isVisible: !this.state.isVisible
        })
    }

    render() {
        const {from} = this.props.location.state || {from: {pathname: '/'}};
        const t = this.props.t;
        if (this.state.redirectToReferrer) {
            return <Redirect to={from}/>
        }
        return (
            <div className="wrapper">
                <form onSubmit={this.onSubmit}>
                    <h1 className="login">{t('MainLayout.Login.header')}</h1>
                    <label htmlFor="username">{t('MainLayout.Login.loginLabel')}</label>
                    <div className="input-wrapper">
                        <input onChange={this.handleInputChange} value={this.state.username}
                               id="username" type="text" placeholder={t('MainLayout.Login.loginPlaceholder')}/>
                    </div>
                    <label htmlFor="password">{t('MainLayout.Login.passwordLabel')}</label>
                    <div className="input-wrapper">
                        <input onChange={this.handleInputChange} value={this.state.password}
                               id="password" type={this.state.isVisible ? "text" : "password"}
                               placeholder={t('MainLayout.Login.passwordPlaceholder')}/>
                        <img src={iconPassword} alt="look at the password" onClick={this.handleIconClick}/>
                    </div>
                    <div className="btn-container">
                        <button className="submit" type="submit">
                            {t('MainLayout.Login.loginButton')}
                        </button>
                        <Link to="/" className="cancel">
                            {t('MainLayout.Login.cancelButton')}
                        </Link>
                    </div>
                </form>
            </div>
        )
    }
}

export default withTranslation()(LoginFormComponent);

LoginFormComponent.propTypes = {
    authActions: PropTypes.object.isRequired
};